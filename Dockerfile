FROM maven:3-jdk-8-slim

RUN mkdir -p /home/dwca
RUN apt-get update && apt-get install -y git jq
RUN cd /opt && git clone https://github.com/tomgilissen/gbif-data-validator.git
RUN cd /opt/gbif-data-validator && mvn clean package install -U
RUN cd /opt/gbif-data-validator/validator-ws

EXPOSE 8080
WORKDIR /opt/gbif-data-validator/validator-ws
CMD mvn jetty:run